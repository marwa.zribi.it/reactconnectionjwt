const express = require('express')
const morgan = require('morgan')
const jwt = require('jsonwebtoken')
const cors = require('cors')

const app = express()

const PORT = 3005
const SECRET = 'mykey'

app.use(cors())                                 // Activation de CORS
app.use(morgan('tiny'))                         // Activation de Morgan
app.use(express.json())                         // Activation du raw (json)
app.use(express.urlencoded({ extended: true })) // Activation de x-wwww-form-urlencoded

// Liste des utilisateurs
const users = [
  {
    email: 'test@gmail.com',
    password: '12345'
  }
]


app.get('*', (req, res) => {
  return res.status(404).json({ message: 'Page not found' })
})

app.post('/login', (req, res) => {
  // Pas d'information à traiter
  if (!req.body.email || !req.body.password) {
    return res.status(400).json({ message: 'Error. Please enter the correct username and password' })
  }

  // Checking
  const user = users.find(u => u.email === req.body.email && u.password === req.body.password)

  // Pas bon
  if (!user) {
    return res.status(400).json({ message: 'Error. Wrong login or password' })
  }

  const token = jwt.sign({
    id: user.id,
    email: user.email
  }, SECRET, { expiresIn: '3 hours' })

  return res.json({ access_token: token })
})


app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
