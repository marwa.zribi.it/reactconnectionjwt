#!/bin/bash
lsof -ti:3005 | xargs kill
lsof -ti:3000 | xargs kill
npm install
cd server/
npm install
node index.js & wait &
cd ../
npm run start &
