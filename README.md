# React Exercice
Temps de Début : Dimanche 5 février à 11h16
Pause à : 14h17
Retour à 16h42


# How To install 
git clone https://gitlab.com/marwa.zribi.it/reactconnectionjwt.git
cd reactconnectionjwt
npm install 
cd server 
npm install
node index.js
cd ..
npm run start

# Install via script shell
git clone https://gitlab.com/marwa.zribi.it/reactconnectionjwt.git
cd reactconnectionjwt
chmod u+x install.sh
./install.sh

# Données du login 
email : test@gmail.com
mot de passe : 12345
# Explication
J'ai créer une application en React JS qui contient 2 écrans : Home + Login 
* J'ai ajouter le react-router-dom pour gérer la navigation entre les pages
* J'ai sécuriser le rendu des pages par une token, Si l'utilisateur est connecté et il a un token en local storage => redirection vers la page home , sinon=> redirection vers la page Login
* J'ai créer un mini-serveur nodejs qui fais le job de génération d'un token JWT et l'envoie au client React.
* J'ai consommé le ws login en fetch et traité les resultats afin de faire la redirection.
* J'ai ajouter react-calendar => plus facile en intégration et fait le travail.
* J'ai effectuer queslques styles css pour faire le design des pages.
* J'ai créer un boutton logout qui à pour role de vider le localstorage et redérige vers l'écran login
* J'ai crée un script shell pour l'installation et le lancement du projet après le clone

J'ai noté les gros pauses, mais j'ai quand même fais quelque autres choses à coté => temps total du travail (5h)