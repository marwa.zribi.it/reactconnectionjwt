import React from "react";
import TextField from '@mui/material/TextField';

class CustomTextInput extends React.Component<{ label: any, onChange:any , type:any, id:any, value: any}> {
    render() {
        let {label, onChange, type, id, value} = this.props;
        return  <TextField id={id} label={label} variant="standard" onChange={onChange} type={type} value={value}/>
    }
}

export default CustomTextInput