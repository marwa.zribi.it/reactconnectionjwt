import {styled} from "@mui/material/styles";
import Button from "@mui/material/Button";
import React from "react";

const StyledButton = styled(Button)({
    padding:"10px 40px",
    borderRadius:10,
    color:'white',
    backgroundColor:'#2C3E50',
    }) as typeof Button
;

class CustomButton extends React.Component<{ content: any, onClick:any }> {
    render() {
        let {content, onClick} = this.props;
        return <StyledButton onClick={onClick}>
            {content}
        </StyledButton>
    }
}

export default CustomButton