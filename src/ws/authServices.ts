const API_URL = "http://localhost:3005/";
export function login( email: any, password: any) {

    return new Promise((dispatch, reject) => {
        let body = {email, password}
        fetch(API_URL + "login", {
            'method': 'POST',
            'headers': {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            'body':  JSON.stringify(body) ,
        }).then(async response => {
            const res = await response;
            let Result = null;
            if (res) {
                try {
                    const Read = await res.text()
                    Result = JSON.parse(Read);
                } catch (e) {
                    Result = await res;
                }
            }
            if(Result.access_token !== undefined && Result.access_token.length !== null){
                return {
                    status : 200,
                    token : Result.access_token
                }
            }
        }).catch((error) => {
            reject(error)

        }).then(function (response) {
            dispatch(response);
        });

    })
}

export function logout() {
    localStorage.removeItem("connectedUserJWT");
}

export function getCurrentUser() {
    // @ts-ignore
    return JSON.parse(localStorage.getItem('connectedUserJWT'));
}
