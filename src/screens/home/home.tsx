import React, {useState} from 'react';
import {Calendar} from 'react-calendar';
import {getCurrentUser, logout} from "../../ws/authServices";
import "./home.css"
import {Navigate} from "react-router-dom";
import CustomButton from "../../components/CustomButton";
const Home = () => {
    const [date, setDate] = useState(new Date());
    const [isLoggedIn, setIsLoggedIn] = useState( getCurrentUser() !== null);
    const onChangeCalendar = (selectedDate:any) => {
        setDate(selectedDate);
    };
    const logoutUser=()=>{
        logout()
        setIsLoggedIn(false)
    }
    return <div className={"home-container"}>{!isLoggedIn?
            <Navigate to={'/login'}/>:<div className={"home-container"}>
        <div className={"user-data"}>
            <div className={"title"}>Utilisateur : </div>
            <div className={"detail"}> Marwa ZRIBI</div>
            <div className={"title"}>Email : </div>
            <div className={"detail"}> marwa.zribi.it@gmail.com</div>
            <div className={"title"}>Token : </div>
            <div className={"detail"}>{getCurrentUser()}</div>
            <CustomButton
                content={"Disconnect"}
                onClick={logoutUser}/>

        </div>
        <div className={"calendar-container"}>
            <Calendar onChange={onChangeCalendar} value={date} />
        </div>
        </div>}</div>;
}
export default Home;