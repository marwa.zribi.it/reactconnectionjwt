import React, {useState} from 'react';
import CustomButton from '../../components/CustomButton'
import CustomTextInput from "../../components/CustomTextInput";
import "./login.css"
import {getCurrentUser, login} from "../../ws/authServices";
import {
    Navigate
} from "react-router-dom";

const Login = () => {
    const [email, setEmail] = useState("");
    const [isLoggedIn, setIsLoggedIn] = useState( getCurrentUser() !== null);
    const [password, setPassword] = useState("");
    const handleLogin = async (e: any) => {
        e.preventDefault();
        if (email.length === 0 || password.length === 0) {
            alert('Email ou mot de passe vide')
            return
        }
        login(email, password).then((data: any) => {
            if (data === undefined ||data.status === undefined || data.status !== 200 ) {
                alert('Email ou mot de passe invalide')
                return
            }
            localStorage.setItem("connectedUserJWT", JSON.stringify(data.token))
            setIsLoggedIn(true)
        });
    }
    return <div>
        {isLoggedIn ?
            <Navigate to={"/"}/>
            :
            <div className={"login-container"}>
                <div className={'login-form'}>
                    <img alt={""} src={"/user.png"} className={"user-logo"}/>
                    <div className={"input-container"}>
                        <CustomTextInput
                            label="Email"
                            type="text"
                            value={email}
                            id={'email'}
                            onChange={(ev: any) => {
                                setEmail(ev.target.value)
                            }}/>
                    </div>
                    <div className={"input-container"}>
                        <CustomTextInput
                            id={'password'}
                            label="Password"
                            type="password"
                            value={password}
                            onChange={(ev: any) => {
                                setPassword(ev.target.value)
                            }}/>
                    </div>
                    <div className={"button-container"}>
                        <CustomButton
                            content={"Se connecter"}
                            onClick={handleLogin}/>
                    </div>

                </div>

            </div>}
    </div>;
}
export default Login;