import * as React from 'react';
import {Route, Routes} from "react-router-dom";
import Home from "./screens/home/home";
import Login from "./screens/login/login";
export default function App() {
    return (
        <Routes>
            <Route path='/' element={<Home/>} />
            <Route path='/login' element={<Login/>} />
        </Routes>
    )
}
